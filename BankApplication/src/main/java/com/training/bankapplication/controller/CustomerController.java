package com.training.bankapplication.controller;
 
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.bankapplication.dto.CustomerDto;
import com.training.bankapplication.dto.ResponseDto;
import com.training.bankapplication.entity.Customer;
import com.training.bankapplication.service.CustomerService;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
 
@RestController
@RequestMapping("/")
@AllArgsConstructor
public class CustomerController {
	
	CustomerService customerService;
	@PostMapping("/customers")
	public ResponseEntity<ResponseDto> addUser(@RequestBody @Valid  CustomerDto customerDto) {
		return new ResponseEntity<>(customerService.addCustomer(customerDto), HttpStatus.CREATED);
	}

	@DeleteMapping("/customers/{customerId}")
	public ResponseEntity<ResponseDto> DeleteUser(@PathVariable @Valid long customerId) {
		return new ResponseEntity<>(customerService.deleteCustomer(customerId), HttpStatus.CREATED);
	}
 
	
	@GetMapping("/customers")
	public ResponseEntity<List<Customer>> getCustomers() {
		return new ResponseEntity<>(customerService.getCustomers(), HttpStatus.CREATED);
	}
	
	@GetMapping("/customer/{customerId}")
	public ResponseEntity<Customer> getCustomer(@PathVariable  long customerId) {
		return new ResponseEntity<>(customerService.getCustomer(customerId), HttpStatus.CREATED);
	}
}

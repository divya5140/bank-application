package com.training.bankapplication.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.bankapplication.dto.TransactionDto;
import com.training.bankapplication.dto.TransactionResponseDto;
import com.training.bankapplication.service.TransactionService;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/transaction")
@AllArgsConstructor
public class TransactionController {
	private TransactionService transactionService;
	@PostMapping
	public ResponseEntity<TransactionResponseDto> fundTransfer(@RequestBody @Valid TransactionDto transactionDto){
		return new ResponseEntity<>(transactionService.fundTransfer(transactionDto),HttpStatus.CREATED);
	}
}

package com.training.bankapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.bankapplication.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}

package com.training.bankapplication.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.bankapplication.entity.Account;
import com.training.bankapplication.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	Customer findByAccount(Account account);
	Optional<Customer> findByEmailId(String emailId);
		Customer findByemailIdAndPassword(String emailId, String password);
}

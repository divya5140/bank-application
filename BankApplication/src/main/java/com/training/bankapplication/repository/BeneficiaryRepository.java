package com.training.bankapplication.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.bankapplication.entity.Beneficiary;

public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Long> {
	List<Beneficiary> findByBeneficiaryCustomerId(long beneficiaryCustomerId);
	
}

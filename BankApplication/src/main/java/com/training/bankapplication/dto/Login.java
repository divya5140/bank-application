package com.training.bankapplication.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record Login(@Email    String emailId,@NotBlank(message = "password is required")  String password) {

}

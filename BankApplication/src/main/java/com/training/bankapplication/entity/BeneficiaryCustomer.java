package com.training.bankapplication.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BeneficiaryCustomer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long beneficiaryCustomerId;
	@ManyToOne(cascade = CascadeType.ALL)
	private Customer customer;
}

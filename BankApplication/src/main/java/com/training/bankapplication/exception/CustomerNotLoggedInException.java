package com.training.bankapplication.exception;

public class CustomerNotLoggedInException extends RuntimeException {

	
	private static final long serialVersionUID = 1L;
	public CustomerNotLoggedInException(String msg) {
		super(msg);
	}
	public CustomerNotLoggedInException() {
		super("Customer Not Logged In Exception");
	}
}

package com.training.bankapplication.exception;

public class CustomerAlreadyExists extends RuntimeException {
	 
	private static final long serialVersionUID = 1L;
 
	public CustomerAlreadyExists(String message) {
		super(message);
	}
 
	public CustomerAlreadyExists() {
		super("Customer already exists");
	}
 
}

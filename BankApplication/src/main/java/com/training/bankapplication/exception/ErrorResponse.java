package com.training.bankapplication.exception;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
	private long errorCode;
	private List<String> errorResponses;
 
	
 
}

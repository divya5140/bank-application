package com.training.bankapplication.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.training.bankapplication.dto.BeneficiaryDto;
import com.training.bankapplication.dto.BeneficiaryResponseDto;
import com.training.bankapplication.entity.Account;
import com.training.bankapplication.entity.Beneficiary;
import com.training.bankapplication.entity.BeneficiaryCustomer;
import com.training.bankapplication.entity.Customer;
import com.training.bankapplication.entity.LoginStatus;
import com.training.bankapplication.exception.AccountNotFoundException;
import com.training.bankapplication.exception.CustomerNotLoggedInException;
import com.training.bankapplication.repository.AccountRepository;
import com.training.bankapplication.repository.BeneficiaryCustomerRepository;
import com.training.bankapplication.repository.BeneficiaryRepository;
import com.training.bankapplication.repository.CustomerRepository;
import com.training.bankapplication.service.BeneficiaryService;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Service
@AllArgsConstructor
@Slf4j
public class BeneficiaryServiceImpl implements BeneficiaryService {
	private AccountRepository accountRepository;
	private CustomerRepository customerRepository;
	private BeneficiaryCustomerRepository beneficiaryCustomerRepository;
	private BeneficiaryRepository beneficiaryRepository;
	@Override
	public BeneficiaryResponseDto addBeneficiary(BeneficiaryDto beneficiaryDto) {
		Account account=accountRepository.findByAccountNumber(beneficiaryDto.getAccountNumber()).orElseThrow(AccountNotFoundException::new);
		Customer customer=customerRepository.findByAccount(account);
		if((customer.getLoginStatus()).equals(LoginStatus.LOGIN)) {
		BeneficiaryCustomer beneficiaryCustomer=beneficiaryCustomerRepository.findByCustomer(customer);	
		beneficiaryRepository.saveAll(beneficiaryDto.getBeneficiaryListDtos().stream().
			map(beneficiary->{
				Beneficiary newbeneficiary=new Beneficiary();
				BeanUtils.copyProperties(beneficiary, newbeneficiary);
				newbeneficiary.setBeneficiaryCustomerId(beneficiaryCustomer.getBeneficiaryCustomerId());
				return newbeneficiary;
			}).toList());	
		log.info("Successfully added beneficiary");
		return new BeneficiaryResponseDto("Successfully added beneficiary", HttpStatus.CREATED.value());
		}
		log.error("Customer Not LoggedIn");
		throw new CustomerNotLoggedInException();
	}

}

package com.training.bankapplication.service;

import java.util.List;

import com.training.bankapplication.dto.CustomerDto;
import com.training.bankapplication.dto.Login;
import com.training.bankapplication.dto.ResponseDto;
import com.training.bankapplication.entity.Account;
import com.training.bankapplication.entity.Customer;
 
public interface CustomerService {
 
	ResponseDto addCustomer(CustomerDto customerDto);
	Account login(Login login);
	ResponseDto deleteCustomer(long customerId);
	List<Customer> getCustomers();
	Customer getCustomer(long customerId);
}
package com.training.bankapplication.service;

import com.training.bankapplication.dto.TransactionDto;
import com.training.bankapplication.dto.TransactionResponseDto;

public interface TransactionService {
	TransactionResponseDto fundTransfer(TransactionDto transactionDto);
}

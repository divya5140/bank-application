package com.training.bankapplication.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.bankapplication.dto.TransactionDto;
import com.training.bankapplication.entity.Account;
import com.training.bankapplication.entity.Beneficiary;
import com.training.bankapplication.entity.BeneficiaryCustomer;
import com.training.bankapplication.entity.Customer;
import com.training.bankapplication.entity.LoginStatus;
import com.training.bankapplication.exception.AccountNotFoundException;
import com.training.bankapplication.exception.CustomerNotLoggedInException;
import com.training.bankapplication.exception.ZeroBalanceException;
import com.training.bankapplication.repository.AccountRepository;
import com.training.bankapplication.repository.BeneficiaryCustomerRepository;
import com.training.bankapplication.repository.BeneficiaryRepository;
import com.training.bankapplication.repository.CustomerRepository;
import com.training.bankapplication.repository.TransactionRepository;

@ExtendWith(SpringExtension.class)
class TransactionserviceImplTest {
	@Mock
	AccountRepository accountRepository;
	@Mock
	CustomerRepository customerRepository;
	@Mock
	BeneficiaryCustomerRepository beneficiaryCustomerRepository;
	@Mock
	BeneficiaryRepository beneficiaryRepository;
	@Mock
	TransactionRepository transactionRepository;
	@InjectMocks
	TransactionServiceImpl transactionServiceImpl;
	@Test
	void testFundTransferSuccess() {
		TransactionDto transactionDto=new TransactionDto(321, 123, 1000);
		Account account=new  Account(1L, 321, 1000,"Saving");
		Customer customer=new Customer(1L, "VAMSHA", "vamshaspoojary@gmail.com", LocalDate.now(), "767676", "v@12", "223e", LoginStatus.LOGIN, account);
		BeneficiaryCustomer beneficiaryCustomer=new  BeneficiaryCustomer(1L, customer);
		Beneficiary beneficiary=new Beneficiary(1L,"sana", 123, "hgygyu", 1L);
		List<Beneficiary> beneficiaries=Arrays.asList(beneficiary);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(Optional.of(account));
		Mockito.when(customerRepository.findByAccount(account)).thenReturn(customer);
		Mockito.when(beneficiaryCustomerRepository.findByCustomer(customer)).thenReturn(beneficiaryCustomer);
		Mockito.when(beneficiaryRepository.findByBeneficiaryCustomerId(anyLong())).thenReturn(beneficiaries);
		assertEquals("Fund transfer successful", transactionServiceImpl.fundTransfer(transactionDto).getMessage());
		
	}
	
	@Test
	void testFundTransferAccountNotFoundException() {
		TransactionDto transactionDto=new TransactionDto(321, 123, 1000);	
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(Optional.empty());
		assertThrows(AccountNotFoundException.class,()->transactionServiceImpl.fundTransfer(transactionDto));
		
	}
	@Test
	void testFundTransferCustomerNotLoggedInException() {
		TransactionDto transactionDto=new TransactionDto(321, 123, 1000);
		Account account=new  Account(1L, 321, 1000,"Saving");
		Customer customer=new Customer(1L, "VAMSHA", "vamshaspoojary@gmail.com", LocalDate.now(), "767676", "v@12", "223e", LoginStatus.LOGOUT, account);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(Optional.of(account));
		Mockito.when(customerRepository.findByAccount(account)).thenReturn(customer);
		assertThrows(CustomerNotLoggedInException.class,()->transactionServiceImpl.fundTransfer(transactionDto));
		
	}
	
	@Test
	void testFundTransferZeroBalanceException() {
		TransactionDto transactionDto=new TransactionDto(321, 123, 1000);
		Account account=new  Account(1L, 321, 0,"Saving");
		Customer customer=new Customer(1L, "VAMSHA", "vamshaspoojary@gmail.com", LocalDate.now(), "767676", "v@12", "223e", LoginStatus.LOGIN, account);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(Optional.of(account));
		Mockito.when(customerRepository.findByAccount(account)).thenReturn(customer);	
		assertThrows(ZeroBalanceException.class,()->transactionServiceImpl.fundTransfer(transactionDto));
		
	}
	
}

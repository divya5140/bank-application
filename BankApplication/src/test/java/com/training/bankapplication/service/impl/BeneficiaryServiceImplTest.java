package com.training.bankapplication.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.bankapplication.dto.BeneficiaryDto;
import com.training.bankapplication.dto.BeneficiaryListDto;
import com.training.bankapplication.entity.Account;
import com.training.bankapplication.entity.Beneficiary;
import com.training.bankapplication.entity.BeneficiaryCustomer;
import com.training.bankapplication.entity.Customer;
import com.training.bankapplication.entity.LoginStatus;
import com.training.bankapplication.exception.AccountNotFoundException;
import com.training.bankapplication.exception.CustomerNotLoggedInException;
import com.training.bankapplication.repository.AccountRepository;
import com.training.bankapplication.repository.BeneficiaryCustomerRepository;
import com.training.bankapplication.repository.BeneficiaryRepository;
import com.training.bankapplication.repository.CustomerRepository;

@ExtendWith(SpringExtension.class)
class BeneficiaryServiceImplTest {
	@Mock
	AccountRepository accountRepository;
	@Mock
	CustomerRepository customerRepository;
	@Mock
	BeneficiaryCustomerRepository beneficiaryCustomerRepository;
	@Mock
	BeneficiaryRepository beneficiaryRepository;
	@InjectMocks
	BeneficiaryServiceImpl beneficiaryServiceImpl;
	
	@Test
	void testAddBeneficiarySuccess() {
		Account account=new Account(1L, 123,1000, "Saving");	
		Customer customer=new Customer(1L, "VAMSHA", "vamshaspoojary@gmail.com", LocalDate.now(), "767676", "v@12", "223e", LoginStatus.LOGIN, account);
		BeneficiaryListDto beneficiaryListDto=new BeneficiaryListDto("Sana", 555, "xyz");
		BeneficiaryDto beneficiaryDto=new BeneficiaryDto(123, Arrays.asList(beneficiaryListDto));
		Beneficiary beneficiary=new Beneficiary();
		BeanUtils.copyProperties(beneficiaryDto, beneficiary);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(Optional.of(account));
		Mockito.when(customerRepository.findByAccount(account)).thenReturn(customer);
		Mockito.when(beneficiaryCustomerRepository.findByCustomer(any())).thenReturn(new BeneficiaryCustomer(1L, customer));
		Mockito.when(beneficiaryRepository.saveAll(anyList())).thenReturn(Arrays.asList(beneficiary));
		assertEquals("Successfully added beneficiary", beneficiaryServiceImpl.addBeneficiary(beneficiaryDto).getMessage());
		assertEquals(201, beneficiaryServiceImpl.addBeneficiary(beneficiaryDto).getStatusCode());	
	}
	
	
	@Test
	void testAddBeneficiaryAccountNotFound() {	
		BeneficiaryListDto beneficiaryListDto=new BeneficiaryListDto("Sana", 555, "xyz");
		BeneficiaryDto beneficiaryDto=new BeneficiaryDto(123, Arrays.asList(beneficiaryListDto));
		Beneficiary beneficiary=new Beneficiary();
		BeanUtils.copyProperties(beneficiaryDto, beneficiary);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(Optional.empty());
		assertThrows(AccountNotFoundException.class,()->beneficiaryServiceImpl.addBeneficiary(beneficiaryDto));
	}
	@Test
	void testAddBeneficiaryNotloggedIn() {
		Account account=new Account(1L, 123,1000, "Saving");	
		Customer customer=new Customer(1L, "VAMSHA", "vamshaspoojary@gmail.com", LocalDate.now(), "767676", "v@12", "223e", LoginStatus.LOGOUT, account);
		BeneficiaryListDto beneficiaryListDto=new BeneficiaryListDto("Sana", 555, "xyz");
		BeneficiaryDto beneficiaryDto=new BeneficiaryDto(123, Arrays.asList(beneficiaryListDto));
		Beneficiary beneficiary=new Beneficiary();
		BeanUtils.copyProperties(beneficiaryDto, beneficiary);
		Mockito.when(accountRepository.findByAccountNumber(anyLong())).thenReturn(Optional.of(account));
		Mockito.when(customerRepository.findByAccount(account)).thenReturn(customer);
		assertThrows(CustomerNotLoggedInException.class, ()->beneficiaryServiceImpl.addBeneficiary(beneficiaryDto));
	}
}

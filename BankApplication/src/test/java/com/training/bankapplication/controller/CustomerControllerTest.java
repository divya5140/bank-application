package com.training.bankapplication.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
 
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
 
import com.training.bankapplication.dto.CustomerDto;
import com.training.bankapplication.dto.ResponseDto;
import com.training.bankapplication.service.CustomerService;
 
@ExtendWith(SpringExtension.class)
class CustomerControllerTest {
	@InjectMocks
	CustomerController customerController;
	@Mock
	CustomerService customerService;

	 @Test
	    void testAddUser_Positive() {
	        CustomerDto customerDto = new CustomerDto();
	        ResponseDto expectedResponse = new ResponseDto();
	        when(customerService.addCustomer(customerDto)).thenReturn(expectedResponse);
	        ResponseEntity<ResponseDto> responseEntity = customerController.addUser(customerDto);
	        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
	        assertEquals(expectedResponse, responseEntity.getBody());
	    }
 
 
}

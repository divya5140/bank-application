package com.training.bankapplication.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.training.bankapplication.dto.TransactionDto;
import com.training.bankapplication.dto.TransactionResponseDto;
import com.training.bankapplication.service.TransactionService;

@ExtendWith(SpringExtension.class)
class TransactionControllerTest {
	@InjectMocks
	TransactionController transactionController;
	@Mock
	TransactionService transactionService;
	 @Test
	    void testAddUser_Positive() {
	        TransactionDto transactionDto=new TransactionDto();
	        TransactionResponseDto transactionResponseDto=new TransactionResponseDto();
	        when(transactionService.fundTransfer(transactionDto)).thenReturn(transactionResponseDto);
	        ResponseEntity<TransactionResponseDto> responseEntity =transactionController.fundTransfer(transactionDto);
	        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
	        assertEquals(transactionResponseDto, responseEntity.getBody());
	    }
}
